## *Yarrowia lipolytica* CLIB 122

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA13837](https://www.ebi.ac.uk/ena/browser/view/PRJNA13837)
* **Assembly accession**: [GCA_000002525.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000002525.1)
* **Original submitter**: Genolevures Consortium

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM252v1
* **Assembly length**: 20,502,981
* **#Chromosomes**: 6
* **Mitochondiral**: No 
* **N50 (L50)**: 3,633,272 (3)

### Annotation overview

* **Original annotator**: Genolevures Consortium
* **CDS count**: 6448
* **Pseudogene count**: 136
* **tRNA count**: 510
* **rRNA count**: 117
* **Mobile element count**: 10
